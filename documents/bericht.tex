\documentclass{article}
\author{Bastian Grahm \and Elena Heier \and Christoph Nagel \and Dominik Schwabe \and Alexander Vopel}
\date{\today}
\title{TED Search Engine}

\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{float}
\usepackage{colortbl}
\usepackage{hyperref}
\usepackage{multirow}

\graphicspath{{./pictures/}}

\begin{document}
	\maketitle
	\newpage
	
	\tableofcontents
	\newpage
	
	\section{Datensatz}
		Der genutzte Datensatz wurde von Rounak Banik erstellt und kann auf \href{https://www.kaggle.com/rounakbanik/ted-talks}{kaggle.com} gefunden werden.
		Es ist zu beachten, dass der Datensatz unter der \href{https://creativecommons.org/licenses/by-nc-sa/4.0/}{CC BY-NC-SA 4.0} Lizenz veröffentlicht wurde.
		Daher steht unsere leicht modifizierte Version des Datensatzes ebenso unter dieser Lizenz. 
		\subsection{Inhalt}
			Der Datensatz besteht ursprünglich aus 2 zusammenhörigen CSV-Dateien
			\textit{ted\_main.csv} und \textit{transcripts.csv}.
			Hierbei befinden sich in \textit{transcripts.csv} die Transkripte aller Talks im Datensatz und in \textit{ted\_main.csv} zu jedem der Talks Metadaten. \\
			Diese sind:
			\begin{description}
				\item[comments] Anzahl der direkten Kommentare auf das Video des Talks
				\item[description] Eine Zusammenfassung des Talks
				\item[duration] Dauer in Sekunden
				\item[event] TED/TEDx event auf dem der Talk gehalten wurde
				\item[film\_date] Unix Zeitstempel des Filmdatums
				\item[languages] Anzahl der Sprachen in denen der Talk verfügbar ist
				\item[main\_speaker] Zuerst genannter Sprecher
				\item[name] Offizieller Name (besteht aus \textit{speaker} und \textit{title})
				\item[num\_speaker] Anzahl der Sprecher
				\item[published\_date] Unix Zeitstempel der Publikation des Videos auf TED.com
				\item[ratings] Dictionary der Bewertungen
				\item[related\_talks] Liste von Dictionaries mit vorgeschlagenen Talks ähnlich zu diesem
				\item[speaker\_occupation] Beruf des \textit{main\_speaker}
				\item[tags] Liste von Themen auf die Bezug genommen wird
				\item[title] Titel
				\item[url] URL (auf TED.com)
				\item[views] Anzahl der Aufrufe des Videos
			\end{description}
		
		\subsection{Modifikation}
			\subsubsection{Bereinigung}
				Da \textit{ted\_main.csv} ganze 2550 Einträge beinhaltet während \textit{transcripts.csv} nur 2467 Einträge hat, ist es offensichtlich, dass es nötig ist die Daten etwas zu bereiningen um einen konsistenten Datensatz vorliegen zu haben.
				
				Daher wurden die überflüssigen 83 Einträge entfernt und da 3 Talks in den Transkripten doppelt vorkamen konnten wir diese ebenfalls löschen.
				
				Es bleiben also 2464 Talks übrig von denen im Datensatz sowohl alle Metadaten als auch die Transkripte vorhanden sind
			\subsubsection{Vereinigung}
				In beiden CSV-Dateien liegen die URLs der Talks vor, sodass es möglich ist jedem Talk in einer Datei eine eineindeutigen Reihe in der anderen Datei zuzuordnen. 
				
				Daher ist es naheliegend die beiden Dateien zu verschmelzen um einerseits Redundanz und zum Anderen einen unnötigen Aufwand durch Auslesen aus mehreren Dateien zum Indexieren zu vermeiden.
				
				Letztendlich bleibt also eine Datei mit allen Metadaten und dem Transkript pro Talk.
			\subsubsection{Nummerierung}
				Um später für die Indexierung eindeutige IDs für jeden Eintrag im Datensatz zu haben ist es noch nötig eine Nummerierung oder Ähnliches durchzuführen.
				
				Um Konsistenz zu wahren und zur Laufzeit keine Fehleranfälligkeit zu bieten haben wir uns entschieden den Datensatz weiter zu manipulieren indem wir einmalig jeder Zeile eine ID zufügten.

	\section{Architektur}
		\subsection{Technologie-Stack}
			\begin{itemize}
				\item Python
				\item Docker
				\item Flask
				\item Elasticsearch
			\end{itemize}
		\subsection{Aufbau}
			\includegraphics[width=\textwidth, angle=0]{flowchart}
			
			Die Anwendung wurde von uns in Python geschrieben. Für das Querying und das Übertragen von Informationen an das Frontend wurde das Flask Framework und die davon genutze Template Sprache Jinja2 genutzt.
			
			Dokumente werden direkt von der Elasticsearch-Python API bulk-indexiert wobei alle vorhandenen Daten in den Index aufgenommen werden. Dadurch ist es uns im Weiteren möglich auf vielen der vorhandenen Feldern zu suchen.
			
			Deployment wird über Docker sichergestellt. Dazu werden 2 Container aufgebaut, wobei einer die grundlegende Anwendung vor allem über Flask verwaltet und der andere den Elasticsearch Index bereit hält.
			So wird über Docker eine Webansicht gehostet, in der ein Benutzer Queries simpel eingeben kann und ihm daraufhin eine Liste von, durch Elasticsearch gefundenen, Ergebnissen angezeigt wird. 
			
			Elasticsearch verwendet in unserer Suchmaschine das Okapi BM25 Retrieval Modell. Hierbei wird in unserem erfolgreichsten Versuch nicht nur auf dem Feld der Transkripte gesucht sondern auch auf \textit{Titeln, Beschreibungen, Sprechern, Beruf des Hauptsprechers, Event und der Tagliste}.
			
			Bei der Suche wird über eine HTTP Post Request die Query an das Backend gestellt und dort direkt an Elasticsearch durchgereicht.
			Dieses liefert dann eine Liste von Ergebnissen vermeintlich relevanter Dokumente zurück.
			Aus den Informationen die hier zurückkommen werden widerrum im Backend Objekte erstellt die über Jinja2 an die Weboberfläche weitergegeben werden. Da wird aus jedem dieser Objekte eine Darstellung eines Suchergebnisses erzeugt, sortiert nach Relevanz.

    \section{Evaluation}
      Zur Evaluierung der Suchmaschine wurden 50 Topics entworfen und pro Topic einige Tedtalks im Bezug auf ihre Relevanz für das Topic bewertet.
      Anhand der Bewertungen wurde versucht die Effektivität der Suchmaschine durch den Entwurf von Analyzern und angepasstem Boosting auf einzelnen indexierten Feldern zu verbessern.
      Zur Bewertung wurden folgende, von Elasticsearch bereitgestellte Metriken benutzt:
      \begin{itemize}
        \item{precision@k}
        \item{MRR@k (mean reciprocal rank)}
        \item{NDCG@k (normalized discounted cumulative gain)}
        \item{ERR@k (expected reciprocal rank)}
      \end{itemize}
      Die Bewertungstiefe k wurde auf 10 festgelegt, da nur 2500 Datensätze indexiert wurden und Anzahl der relevanten Datensätze pro Topic demzufolge gering ist, wobei das auch von der Spezifität des Topic abhängt.
      Um die Effektivität besser bewerten zu können wurde eine zweite Suchmaschine gebaut, welche statt einem BM25 Modell ein LSI Modell benutzt.

    \subsection{Topicdefinition}
      Die Topics sind in der Datei \texttt{searchengine/app/init\_topics.py} definiert.
      Daraus werden die Topic-Dateien abgeleitet, welche in \texttt{searchengine/topics} gespeichert sind.
      Eine Topic-Datei hat dabei den Aufbau:
        \begin{itemize}
          \item{id}
          \item{query}
          \item{description}
          \item{relevances (Dictionary bewerteter Tedtalks mit ihrere Bewertung (1/0))}
        \end{itemize}
      Bei der Bewertung der Topics über \texttt{localhost:5000/eval} wird das Feedback direkt in die Dateien geschrieben und in die Git-Versionierung aufgenommen.
      Dadurch kann die Evaluierung sehr einfach auf die Gruppenmitglieder aufgeteilt werden.

    \subsection{Konfigurationen}
      Die Konfiguration des Index, der Evaluierten Analyzer und der Querysprach können in der Datei \texttt{searchengine/app/constants.py} nachgelesen werden.
      Die Analyzer haben hierbei die selben Namen, wie in den folgenden Grafiken.

	  \subsection{Test verschiedener Analyzer}
      \begin{figure}[H]
        \includegraphics[width=\linewidth]{evaluation_analyzer} \\
        \caption{Test verschiedener Analyzer}
        \label{analyzer}
      \end{figure}
      In Abbildung \ref{analyzer} werden zuerst Evaluationen verschiedener Elasticsearch Analyzer gezeigt.
  		Ein Analyzer stellt in Elasticsearch ein konfigurierbares Konstrukt dar, welches \textit{Character Filter, Tokenizer, Token Filter (Stopper, \dots), Stemmer} etc. beinhaltet.
	  	Dieser wird beim Indexieren auf die gewünschten Felder angewandt und zur Laufzeit ebenso auf die Query.
      \\
      Die Baseline stellt hier der \href{https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-standard-analyzer.html}{\textit{standard} Analyzer} von Elasticsearch. Einen weiteren wichtigen Analyzer für unseren Datensatz stellt der \href{https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-lang-analyzer.html#english-analyzer}{\textit{english} Analyzer}, welcher ebenfalls von Elasticsearch vorgefertigt wurde, dar, da sich unsere Suche quasi ausschließlich auf englische Texte bezieht.
      
      Alle anderen hier zu beobachtenden Analyzer sind manuell von uns erstellt worden. Die Konfigurationen sind \href{https://git.informatik.uni-leipzig.de/bg40lecu/ir_tt/blob/master/searchengine/app/constants.py}{hier} ersichtlich.
      
      Der \textit{own\_english} Analyzer unterscheidet sich vom \textit{english} Analyzer nur in einer erweiterten Stoppwortliste, wodurch offenbar keine großartige Verbesserung erzielt werden kann.
      
      Während sich bei \textit{tedanalyzer2} durch Hinzufügen eines Stoppers im Vergleich zu \textit{tedanalyzer1} beinahe keine Verbesserung in den Metriken ergibt ist das Zufügen eines Stemmers anstelle eines Stoppers in \textit{tedanalyzer3} mit einer deutlichen Steigerung in allen Metriken belohnt worden.
		
	    Ein experimenteller Ansatz mittels \href{https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-common-grams-tokenfilter.html}{\textit{Common Grams} Token Filter} (\textit{tedanalyzer4, tedanalyzer5}) konnte wegen mangelhafter Ergebnisse direkt wieder verworfen werden.
	    
	    Zuletzt versuchten wir eine Verbesserung durch \href{https://www.elastic.co/guide/en/elasticsearch/reference/current/analysis-shingle-tokenfilter.html}{\textit{Trigram} Token Filter} zu erreichen. Hier stechen \textit{trigram2} und \textit{trigram5} besonders hervor, welche sich widerrum durch die Benutzung eines Stemmers definieren. Wobei \textit{trigram5} zusätzlich einen Stopper verwendet. 
	   	\\
	   	Generell lässt sich beobachten, dass eine Stemmernutzung für optimale Ergebnisse beinahe obligatorisch zu sein scheint, während andere Änderungen das Ergebnis nur leicht zu beeinflussen scheinen.
	  	\\
      In Tabelle \ref{table.analyzer} kann die Effektivität des Standard-Analyzers und der besten Analyzer aus Abbildung \ref{analyzer} genauer miteinander verglichen werden.
      \begin{table}[H]
        \begin{center}
          \begin{tabular}{|l|l|l|l|l|}
          \hline
           & precision@10 & NDCG@10 & MRR@10 & MRR@10 \\
          \hline
          standard & 0.313 & 0.460 & 0.435 & 0.671 \\
          own\_english & 0.353 & 0.499 & 0.474 & 0.757 \\
          tedanalyzer3 & 0.351 & 0.499 & 0.476 & 0.760 \\
          trigram5 & 0.349 & 0.498 & 0.468 & 0.745 \\
          \hline
          \end{tabular}
          \caption{Vergleich des Standard-Analyzers und der besten Analyzer aus Abbildung \ref{analyzer}}
          \label{table.analyzer}
        \end{center}
      \end{table}
  
  	  \subsection{Test verschiedener Queryboosts}
        \begin{figure}[H]
          \includegraphics[width=\linewidth]{evaluation_query} \\
          \caption{Test verschiedener Queryboosts}
          \label{queryboost}
        \end{figure}
  	    In diesem Abschnitt (siehe Abbildung \ref{queryboost}) haben wir uns der Verbesserung von Suchergebnissen durch Boosts auf den indexierten Feldern gewidmet.
        Hier stellt \textit{transcripts} unsere Baseline dar, da wir hier nur auf einem Feld, namentlich den Transkripten suchen. Denn dieses beinhaltet das breiteste Spektrum an Informationen über einen Talk.
        Desweiteren entspricht \textit{transcripts} hier dem besten Graph aus Abbildung \ref{analyzer}.
        
        Die naheliegendste Verbesserung davon ist natürlich, die Suche nicht nur auf dieses Feld zu beschränken, sondern auch auf vielversprechendere Felder wie beispielsweise die Tagliste, welche viel kondensiertere Informationen verspricht.
        So wurde bei \textit{multiple fields (no boost)} (wie schon eher kurz beschrieben) zusätzlich auf den Feldern \textit{title, description, main\_speaker, speaker\_occupation, event und tags} gesucht.
        Jedoch wie der Name schon verrät ohne Boosting auf einem dieser Felder.
        
        In den folgenden Versuchen wurde dann probiert über Boosting weiter zu verbessern jedoch nur mit mäßigem Erfolg.
        
        Da nun schon sehr viele Talks zu allen Topics evaluiert waren ließ sich auch bei minimaler Veränderung der Boosting-Werte eine Verbesserung abschätzen, sodass wir diese Hyperparameter kleinschrittig anpassen konnten und so erreichten wir weitere Steigerung von NDCG und Precision. (\textit{multiple fields (best ndcg)}, \textit{multiple fields (best precision)}

      \subsection{Evaluierung mittels LSI-Index}
      Um die Qualität der Suchmaschine besser einschätzen zu können wurde eine zweite Suchmaschine entworfen, welche als Retrieval Modell Latent-Semantic-Indexing verwendet.
      Dieses Modell eignet sich besonders für die Domäne der Tedtalks, auf Grund der Wortvielfalt. So kann es zum Beispiel vorkommen, dass ein Redner über ein Theme spricht, welches zum relevant für eine Query ist, aber keine Worte verwendet, welche in der Query vorkommen.
      Das LSI-Modell kann solche semantischen Zusammengehörigkeiten erkennen und inhaltlich ähnliche Tedtalks besser clustern.
      Folglich ist eine höhere Precision erwartbar, als bei dem auf BM25 basierendem Elasticsearch.
      Gebaut wurde der Index mit der Python-Bibliothek ``gensim'' und kann unter folgendem Link gefunden werden \href{https://git.informatik.uni-leipzig.de/ds40bamo/ir\_tt<Paste>}{https://git.informatik.uni-leipzig.de/ds40bamo/ir\_tt}
			\subsection{Optimaler LSI-Index}
				Experimente haben gezeigt, dass eine Lemmatisierung des Textes keine Vorteile hat, da folglich auch die Query lemmatisiert werden muss.
				Hierzu sind die POS-Tags der Worte nötig. Da Queries in der Regel sehr simpel und grammatikalisch unvollständig formuliert werden, können die POS-Tags der Query-Tokens nicht eindeutig bestimmt werde, was bestimmte Worte für die Query unbrauchbar macht.
				Sowohl ein Stopping häufiger Worte mittels der english-stopwords von python-nltk und ein anschließendes Stemming konnte die Retrieval Qualität LSI-Suchmaschine verbessern.
				Zusätzlich konnte eine Verbesserung der Precision durch entfernen der 30 häufigsten Token konnte die Retrieval Qualität weiter verbessert werden.
        \begin{figure}[H]
					\includegraphics[width=\linewidth]{evaluation_lsi} \\
					\caption{Effektivität das LSI-Index bei entsprechender Topicanzahl}
					\label{lsi_topics}
				\end{figure}
				In Abbildung \ref{lsi_topics} kann die Retrieval Qualität in Abhängigkeit von der Topicanzahl erkannt werden.
				Dabei ist zu erkennen, dass bei einer Wahl von 100 Topics bessere Ergebnisse erzielt werden, als bei einer Wahl von halb so vielen oder mehr als doppelt so vielen.
				Es ist unklar ob bei 100 das Optimum $o$ liegt oder ob $o \in (50,200) \setminus {100}$.
			\subsection{Vergleich BM25 - LSI}
        \begin{figure}[H]
					\includegraphics[width=\linewidth]{evaluation_lsi_elasticsearch} \\
					\caption{Vergleich Elasticsearch - LSI}
					\label{lsi_vs_elasticsearch}
				\end{figure}
				In Abbildung \ref{lsi_vs_elasticsearch} ist erkennbar, dass die auf LSI basierte Suchmaschine eine bessere Precision aufweist als die auf Elasticsearch basierende Suchmaschine.
				Erklärt werden kann dies dadurch, dass die LSI-Suchmaschine bestimmte Topics erkennen kann, für die die relevanten Tedtalks nicht die selben Worte enthalten, wie die Query.
				Erkennbar wird dies anhand von den Tabellen \ref{table.badtopics.bm25} und \ref{table.badtopics.lsi}.

				Es ist anzunehmen, dass es für Topic 13 und 15 keine relevanten Ergebnisse gibt, da hier beide Suchmaschinen keine relevanten Ergebnisse finden konnten.

				Bei Topic 14 und 16 schneidet die LSI-Suchmaschine sehr viel besser ab als die Elasticsearch-Suchmaschine, weil der Nutzer hier seinen Information Need beschreibt, als ihn durch in relevanten Dokumenten vorkommende Worte zu repräsentieren.

				In Abbildung \ref{lsi_vs_elasticsearch} ist zudem erkennbar, dass die LSI-Suchmaschine einen schlechteren mean reciprocal rank und somit auch einen schlechteren NDCG aufweist, als die Elasticsearch-Suchmaschine.
				Erklärt werden kann dies dadurch, dass das erste Ergebnis, welches Elasticsearch liefert, zu einer hohen Wahrscheinlichkeit relevant ist, da es das Ergebnis ist, welches die meisten der in der Query vorkommenden Worte in hoher Anzahl enthält.
				Dies ist besonderes dann der Fall, wenn der Information Need mit ausreichend Schlagwörtern definiert wurde.

			\begin{table}
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					\textbf{id} & \textbf{name} & \textbf{precision@10} & \textbf{NDCG@10} & \textbf{MRR@10} \\
					\hline
					15 & living in a religious cult          & 0.0 & 0.0 & 0.0 \\
					13 & artificially generated art          & 0.0 & 0.0 & 0.0 \\
					14 & How to be funny                     & 0.1 & 0.2 & 0.0851 \\
					16 & motivate to buy                     & 0.1 & 0.2 & 0.117 \\
					22 & Internet influence on everyday life & 0.1 & 0.5 & 0.139 \\
					\hline
				\end{tabular}
				\caption{5 Topics mit der schlechtesten Effektivität in Elasticsearch im Kontext von Elasticsearch }
				\label{table.badtopics.bm25}
			\end{table}

			\begin{table}
				\begin{tabular}{|l|l|l|l|l|}
					\hline
					\textbf{id} & \textbf{name} & \textbf{precision@10} & \textbf{NDCG@10} & \textbf{MRR@10} \\
					\hline
					15 & living in a religious cult          & 0.0   & 0.0   & 0.0 \\
					13 & artificially generated art          & 0.0   & 0.0   & 0.0 \\
					14 & How to be funny                     & 0.444 & 1.0   & 0.423 \\
					16 & motivate to buy                     & 0.4   & 1.0   & 0.593 \\
					22 & Internet influence on everyday life & 0.222 & 0.143 & 0.143 \\
					\hline
				\end{tabular}
				\caption{5 Topics mit der schlechtesten Effektivität in Elasticsearch im Kontext von LSI}
				\label{table.badtopics.lsi}
			\end{table}

 
		\section{Fazit}
      \begin{table}[H]
        \begin{center}
          \begin{tabular}{|l|l|l|l|l|}
            \hline
            \textbf{Projekt Phase} & \textbf{precision@10} & \textbf{MRR@10} & \textbf{NDCG@10} \\
            \hline
            Anfang                                & 0.313         & 0.671  & 0.46    \\
            \multirow{2}{*}{Ende (Elasticsearch)} & 0.379         & 0.778  & 0.530   \\
                                                  & 0.389         & 0.744  & 0.531   \\
            Ende (LSI)                            & 0.413         & 0.713  & 0.436   \\
            \hline
          \end{tabular}
          \caption{Vergleich Projektanfang und Projektende}
          \label{table.projekt}
        \end{center}
			\end{table}

      Aus Tabelle \ref{table.projekt} geht hervor, dass durch den Einsatz eines angepassten Analyzers und einer geeigneten Query die Effektivität der Suchmaschine gesteigert werden konnte.
      In der auf Elasticsearch basierenden Suchmaschine konnte eine Steigerung der Precision@10 um ca. 8\% und des NDCG um 6\% erreicht werden.
      In der auf LSI basierenden Suchmaschine konnte die Precision@10 sogar um 10\% gesteigert werden.
      Der NDCG@10 hat hiebei um 2\% abgenommen, wobei dies auch auf Implementierungsfehler zurückzuführen sein könnte.
\end{document}
