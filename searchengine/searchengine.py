from app import app, constants
from app.evaluation import Topic
from pprint import pprint

def search(query, start=0, size=constants.k):
    query_template = constants.query_template
    query_template["_source"] = ''
    body = {
            "source": query_template,
            "params": {
                "query_string": query,
                "from": start,
                "size": size,
                }
            }
    pprint(app.elasticsearch.search_template(index=constants.indexname, body=body))


def analyze(body):
    result = app.elasticsearch.indices.analyze(index=constants.indexname, body=body)
    transform = [e["token"] for e in result["tokens"]]
    print(body["text"])
    print()
    print(transform)

def suggest(text):
    body = {
        "_source": "",
        "suggest": {
            "test" : {
                "prefix" : text,
                "completion" : {
                    "field" : "suggest",
                    "size": 5,
                }
            }
        }
    }
    pprint(app.elasticsearch.search(index=constants.indexname, body=body))


testtext ="Good morning. How are you?(Laughter)It's been great, hasn't it? I've been blown away by the whole thing. In fact, I'm leaving.(Laughter)There have been three themes running through the conference which are relevant to what I want to talk about. One is the extraordinary evidence of human creativity in all of the presentations that we've had and in all of the people here. Just the variety of it and the range of it. The second is that it's put us in a place where we have no idea what's going to happen, in terms of the future. No idea how this may play out.I have an interest in education. Actually, what I find is everybody has an interest in education. Don't you? I find this very interesting. If you're at a dinner party, and you say you work in education — Actually, you're not often at dinner parties, frankly.(Laughter)If you work in education, you're not asked.(Laughter)And you're never asked back, curiously. That's strange to me. But if you are, and you say to somebody, you know, they say, \"What do you do?\" and you say you work in education, you can see the blood run from their face. They're like, \"Oh my God,\" you know, \"Why me?\"(Laughter)\"My one night out all week.\""

@app.shell_context_processor
def make_shell_context():
    return {'es': app.elasticsearch, 'indexname': constants.indexname, 'search': search, 'analyze': analyze, 'suggest': suggest, 'testtext': testtext, 'Topic': Topic, 'pprint': pprint}
