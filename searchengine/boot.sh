#!/bin/sh

pipenv install
/wait/wait-for-it.sh -t 0 index:9200
pipenv run sh -c "flask init-index-if-not-exists && \
                  flask run -h 0.0.0.0"
