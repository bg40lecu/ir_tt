import os

class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'sjfdlasjdflajdsf'
    WTF_CSRF_ENABLED = False                  # protect against csrf-attack
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL') or 'http://localhost:9200'
