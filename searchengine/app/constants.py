indexname = "tedtalks"
k = 10
settings = {
    "analysis": {
        "analyzer": {
            "tedanalyzer1": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                ],
            },
            "tedanalyzer2": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedstopper",
                ],
            },
            "tedanalyzer3": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedstopper",
                    "tedstem",
                ],
            },
            "tedanalyzer4": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedcommon_grams", # requires no stopping
                ],
            },
            "tedanalyzer5": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedcommon_grams", # requires no stopping
                    "tedstem",
                ],
            },
            "trigram1": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "3gram",
                ],
            },
            "trigram2": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedstem",
                    "3gram",
                ],
            },
            "trigram3": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "3gram",
                    "tedstopper",
                ],
            },
            "trigram4": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedstopper",
                    "3gram",
                ],
            },
            "trigram5": {
                "type": "custom",
                "tokenizer": "tedtokenizer",
                "filter": [
                    "asciifolding",
                    "lowercase",
                    "tedstopper",
                    "tedstem",
                    "3gram",
                ],
            },

            # english analyzer rebuild
            "own_english": {
                "type": "custom",
                "tokenizer": "standard",
                "filter": [
                    "own_english_possessive_stemmer",
                    "lowercase",
                    "own_english_stop",
                    "own_english_stemmer"
                    ]
            }
        },
        "tokenizer": {
            "tedtokenizer": { "type": "char_group", "tokenize_on_chars": ["whitespace", "punctuation", "symbol"], },
        },
        "filter": {
            "tedstopper": {
                "type": "stop",
                "stopwords": ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
            },
            "tedstem": {
                "type": "snowball",
                "language": "english",
            },
            "tedcommon_grams": {
                "type": "common_grams",
                "common_words": "_english_",
            },
            "3gram": {
                "type": "shingle",
                "min_shingle_size": 2,
                "max_shingle_size": 3,
            },

            # filters for english analyzer rebuild
            "own_english_stop": {
                "type": "stop",
                "stopwords": ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]
            },
            "own_english_stemmer": {
                "type": "stemmer",
                "language": "english"
            },
            "own_english_possessive_stemmer": {
                "type": "stemmer",
                "language": "possessive_english"
            }
        },
    }
}
mapping = {
    "properties": {
        "transcript": {
            "type": "text",
            "analyzer": "own_english",
        },
        "name": {
            "type": "text",
            "analyzer": "own_english"
        },
        "title": {
            "type": "text",
            "analyzer": "own_english"
        },
        "decription": {
            "type": "text",
            "analyzer": "own_english"
        },
        "speaker": {
            "type": "text",
            "analyzer": "own_english"
        },
        "speaker_occupation": {
            "type": "text",
            "analyzer": "own_english"
        },
        "num_speaker": { "type": "integer" },
        "url": { "type": "keyword" },
        "thumbnail_url": { "type": "keyword" },
        "event": { "type": "text" },
        "tags": {
            "type": "text",
            "analyzer": "own_english"
            },
        "views": { "type": "integer" },
        "comments": { "type": "integer" },
        "duration": { "type": "integer" },
        "languages": { "type": "integer" },
        "film_date": {
            "type": "date",
            "format": "epoch_second",
        },
        "published_date": {
            "type": "date",
            "format": "epoch_second",
        },
        "ratings": {
            "type": "nested",
            "dynamic": False,
            "properties": {
                "id": { "type": "integer" },
                "name": { "type": "keyword" },
                "count": { "type": "integer" },
            },
        },
        "related_talks": {
            "type": "nested",
            "dynamic": False,
            "properties": {
                "name": { "type": "keyword" },
                "count": { "type": "integer" },
            },
        },
    }
}
base_template_1 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                ]
            }
        }
base_template_2 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title",
                "description",
                "speaker",
                "speaker_occupation",
                "event",
                "tags"
                ]
            }
        }
base_template_3 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title^1.5",
                "description^1.5",
                "speaker^3",
                "speaker_occupation^2",
                "event^3",
                "tags^2"
                ]
            }
        }
base_template_4 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title^2",
                "description",
                "speaker^3",
                "speaker_occupation^2",
                "event^3",
                "tags^3"
                ]
            }
        }
base_template_5 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title^0.5",
                "description^0.5",
                "speaker^3",
                "speaker_occupation^2",
                "event^3",
                "tags^0.5"
                ]
            }
        }
base_template_6 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title",
                "description",
                "speaker^3",
                "speaker_occupation",
                "event^3",
                "tags^4"
                ]
            }
        }
base_template_7 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title^0.3",
                "description^0.3",
                "speaker^3",
                "speaker_occupation^0.3",
                "event^3",
                "tags^0.3"
                ]
            }
        }
base_template_8 = {
        "multi_match": {
            "type": "most_fields",
            "query": "{{query_string}}",
            "fields": [
                "transcript",
                "title^0.3",
                "description^0.3",
                "speaker^3",
                "speaker_occupation^0.2",
                "event^3",
                "tags^0.5"
                ]
            }
        }
base_template = base_template_8
eval_templates = [
    {
        "id": "1",
        "template": {
            "inline": {
                "query": base_template
            }
        }
    }
]
query_template = {
        "from": "{{ from }}",
        "size": "{{ size }}",
        "query": base_template,
        "highlight": {
            "fields": {
                "transcript": {}
                },
            "fragment_size": 150,
            "number_of_fragments": 5,
            },
        }
