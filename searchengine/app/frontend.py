class FrontendObject:
    # TODO: Add possibly useful metadata information
    def __init__(self, tid, title, score, ranking,
            raw_script, snippet, embedding, thumbnail_url, event):
        self.tid = tid
        self.title = title
        self.score = score
        self.ranking = ranking
        self.raw_script = raw_script
        self.snippet = snippet
        self.embedding = embedding
        self.thumbnail_url = thumbnail_url
        self.event = event


def embedding_generator(url):
    parts = url.split('.')
    parts[0] = 'https://embed'
    return '.'.join(parts)


def response_list_constructor(response):
    response_list = []
    ranking_counter = 0
    for hit in response['hits']['hits']:
        source = hit['_source']
        ranking_counter += 1
        response_object = FrontendObject(
            tid=hit['_id'],
            title=source['name'],
            score=hit['_score'],
            ranking=ranking_counter,
            raw_script=source['transcript'],
            snippet='<br>'.join(hit['highlight']['transcript']) if hit.get('highlight', False) else source['description'],
            embedding= embedding_generator(source['url']),
            thumbnail_url=source['thumbnail_url'],
            event=source['event']
        )

        response_list.append(response_object)

    return response_list
