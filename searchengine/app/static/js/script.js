function open_overlay(title, script, url){
    $("#overlay_title").text(title)
    $("#overlay_script").text(script)
    $("#overlay_player").html('<iframe id="embedding" class="embed-responsive-item" src=' + url + ' frameborder="0" allowfullscreen></iframe>')
}

function remove_content(){
    console.log("remove")
    $("#overlay_title").text("")
    $("#overlay_script").text("")
    $("#overlay_player").html("")
}

function video(){
    $(".Video").addClass("active");
    $(".Script").removeClass("active");
    $(".Video_and_Script").removeClass("active");
    $(".videoSizer").show();
    $(".scriptSizer").hide();
}

function script(){
    $(".Video").removeClass("active");
    $(".Script").addClass("active");
    $(".Video_and_Script").removeClass("active");
    $(".scriptSizer").show();
    $(".pre-scrollable").css("cssText", "min-height: 50vh !important;");
    $(".videoSizer").hide();
}

function script_and_video(){
    $(".Video").removeClass("active");
    $(".Script").removeClass("active");
    $(".Video_and_Script").addClass("active");
    $(".scriptSizer").show();
    $(".videoSizer").show();
    $(".pre-scrollable").css("cssText", "min-height: 10vh !important;");
}
