import json
import click
import os
from app import app, constants
from collections import defaultdict
from os import path
from datetime import datetime

class Topic:

    topics_path = path.join(path.dirname(path.realpath(__file__)), '..', 'topics')
    topic_path_template = path.join(topics_path, 'topic_{:0>2}.json')

    def __init__(self, id, query, description, relevances={}):
        self.id = id
        self.query = query
        self.description = description
        self.relevances = relevances
        self.path = Topic.topic_path_template.format(id)

    def save(self):
        with open(self.path, 'w') as topic_file:
            topic = self.to_dict()
            json.dump(topic, topic_file, indent=4, ensure_ascii=False)

    @staticmethod
    def load_by_id(topic_id):
        topic_path = Topic.topic_path_template.format(topic_id)
        return Topic.load_by_file_name(topic_path)

    @staticmethod
    def load_by_file_name(file_name):
        with open(file_name, 'r') as file:
            topic_dict = json.load(file)
            return Topic(topic_dict['id'],
                         topic_dict['query'],
                         topic_dict['description'],
                         topic_dict['relevances'])

    def add_relevances(self, relevances):
        relevances = {id: int(relevance) for id, relevance in relevances.items() if int(relevance) > -1}
        self.relevances.update(relevances)

    def to_dict(self):
        topic = {
            'id': self.id,
            'query': self.query,
            'description': self.description,
            'relevances': self.relevances
        }
        return topic


def load_all_topics():
    all_topics = {}
    topics_path = Topic.topics_path
    for element in os.listdir(topics_path):
        file_name = path.join(topics_path, element)
        if path.isfile(file_name):
            try:
                topic = Topic.load_by_file_name(file_name)
                all_topics[topic.id] = topic
            except Exception:
                pass
    return all_topics


@app.cli.command(help="evaluate search effectiveness on topics")
@click.option("--sort", type=int, default=1, help="column number for sorting")
@click.option("--save", help="save the outcome to evaluation file and name tag it with the given name")
def evaluate(sort, save):
    # metrics to calculate
    metrics = {
        "precision" : {
            "altname": "precision",
            "relevant_rating_threshold": 1,
            "ignore_unlabeled": True,
        },
        "mean_reciprocal_rank": {
            "altname": "mean reciprocal rank",
            "relevant_rating_threshold" : 1,
        },
        "dcg": {
            "altname": "ndcg",
            "normalize": True,
        },
        "expected_reciprocal_rank": {
            "altname": "expected reciprocal rank",
            "maximum_relevance" : 1,
       }
    }

    table = EvalTable(metrics, constants.k)

    table.sort(sort)
    table.print()

    if save:
        table.save(save)


class EvalTable:
    def __init__(self, metrics, k=10):
        self.k = k
        for _, details in metrics.items():
            details["k"] = self.k
        self.metricnames = [metricinfo["altname"] for metricinfo in metrics.values()]
        self.static_tablehead = ["topicID", "# unrated documents", "# hits"]
        self.dynamic_tablehead = [metricname + "@" + str(self.k) for metricname in self.metricnames]
        self.scores = {}
        self.topics = defaultdict(lambda: {"scores": {}, "hits": set(), "unrated_docs": set()})
        self.rows = []
        self.unrated_docs = {}

        body = EvalTable.build_query()
        for metricname, description in metrics.items():
            altname = description["altname"]
            del description["altname"]
            body["metric"] = { metricname : description }
            response = app.elasticsearch.rank_eval(index=constants.indexname, body=body)

            self.scores[altname] = response["metric_score"]

            details = response["details"]
            for id, detail in details.items():
                id = int(id)
                hits = [hit["hit"]["_id"] for hit in detail["hits"]]
                unrated_docs = [int(doc["_id"]) for doc in detail["unrated_docs"]]
                self.topics[id]["scores"][altname] = detail["metric_score"]
                self.topics[id]["hits"] |= set(hits)
                self.topics[id]["unrated_docs"] |= set(unrated_docs)

        for id, info in self.topics.items():
            hits = info["hits"]
            unrated_docs = info["unrated_docs"]
            self.unrated_docs[id] = unrated_docs
            self.rows.append([id, len(unrated_docs), len(hits), *[info["scores"][metricname] for metricname in self.metricnames]])

    @staticmethod
    def build_query():
        requests = []
        for _, topic in load_all_topics().items():
            ratings = [{"_index": constants.indexname, "_id": doc_id, "rating": rating} for doc_id, rating in topic.relevances.items()]
            query = {
                "id": str(topic.id),
                "ratings": ratings,
                "template_id": "1",
                "params": {
                    "field": "transcript",
                    "query_string": topic.query,
                }
            }
            requests.append(query)
        body = {
            "templates": constants.eval_templates,
            "requests": requests,
        }

        return body

    def sort(self, colnum):
        self.rows = sorted(self.rows, key=lambda row: row[colnum-1], reverse=True)

    def save(self, name):
        topics = {}
        for id, details in self.topics.items():
            topics[id] = {}
            topics[id]["scores"] = details["scores"]
            topics[id]["hits"] = len(details["hits"])
            topics[id]["unrated_docs"] = len(details["unrated_docs"])

        eval_info = {
            "name": name,
            "k": self.k,
            "metrics": self.scores,
            "topics": topics,
        }

        eval_path = path.join(Topic.topics_path, 'evaluation.json')

        data = {}
        try:
            with open(eval_path, 'r') as file:
                data = json.load(file)
        except Exception:
            pass

        time = datetime.utcnow().timestamp()
        data[time] = eval_info

        with open(eval_path, 'w') as file:
            json.dump(data, file, indent=4, ensure_ascii=False)


    def print(self):
        static_widths = [len(e) for e in self.static_tablehead]
        dynamic_widths = [len(e) for e in self.dynamic_tablehead]

        fmt = "|"
        for width in static_widths:
            fmt += " {:<" + str(width) + "} |"

        for width in dynamic_widths:
            fmt += " {:<" + str(width) + ".3} |"

        # print average scores
        print("-"*67)
        for metricname in self.metricnames:
            print("{:40} {:.3}".format("average {}:".format(metricname + "@" + str(self.k)), self.scores[metricname]))
        print("-"*67)

        # print info for each topic
        tablehead = "| " + " | ".join(self.static_tablehead + self.dynamic_tablehead) + " |"
        print(tablehead)

        for row in self.rows:
            print(fmt.format(*row))
        print("-"*67)

        # print ids of unrated docs
        unrated_docs = [(int(id), docs) for id, docs in self.unrated_docs.items()]
        unrated_docs.sort()
        for id, docs in unrated_docs:
            print("unrated docs for Topic {}: {}".format(id, sorted(docs)))

