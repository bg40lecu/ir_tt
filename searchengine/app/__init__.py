from flask import Flask
from config import Config
from elasticsearch import Elasticsearch

app = Flask(__name__)
app.config.from_object(Config)

app.elasticsearch = Elasticsearch([app.config['ELASTICSEARCH_URL']], timeout=100)

from app import routes, data, indexing, evaluation, init_topics, constants
