from flask import request
from flask_wtf import FlaskForm
from wtforms import StringField, IntegerField
from wtforms.validators import DataRequired



class QueryForm(FlaskForm):
    query = StringField('Query', validators=[DataRequired()])
    page = IntegerField('Page', validators=[DataRequired()])

    def __init__(self, *args, **kwargs):
        kwargs['formdata'] = request.args
        kwargs['csrf_enabled'] = False
        super(QueryForm, self).__init__(*args, **kwargs)


class TopicQueryForm(FlaskForm):
    topic_id = StringField('Query', validators=[DataRequired()])
