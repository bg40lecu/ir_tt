from app import app
from app import constants


def create_index(index):
    app.elasticsearch.indices.create(index=index)


def delete_index(index):
    app.elasticsearch.indices.delete(index=index)


def index_exists(index):
    return app.elasticsearch.indices.exists(index=index)


def update_mapping(index):
    app.elasticsearch.indices.put_mapping(index=index, body=constants.mapping)


def update_settings(index):
    app.elasticsearch.indices.close(index=index)
    app.elasticsearch.indices.put_settings(index=index, body=constants.settings)
    app.elasticsearch.indices.open(index=index)


def bulk_index(index, instances):
    body = []
    for instance in instances:
        op_dict = {
            "index": {
                "_id": instance["id"],
            }
        }
        del instance["id"]

        body.append(op_dict)
        body.append(instance)

    app.elasticsearch.bulk(index=index, body=body)


def add_to_index(index, instance):
    """
    Indexing an instance by adding it to a given elasticsearch index
    :param index: The elasticsearch index to add to
    :param instance: The instance referencing to one in the SQL-database that shall be indexed
    :return: Failure return if no elasticsearch object active
    """

    if not app.elasticsearch:
        return

    app.elasticsearch.index(index=index, id=instance.id, body=instance)


def remove_from_index(index, instance):
    """
    Making an instance unsearchable by removing it from the given index
    :param index: The elasticsearch index to remove from
    :param instance: The instance referencing to one in the SQL-database that shall be removed
    :return:
    """

    if not app.elasticsearch:
        return
    app.elasticsearch.delete(index=index, doc_type=index, id=instance.id)


def query_index(index, query, page=1):
    if not app.elasticsearch:
        return

    body = {
            "source": constants.query_template,
            "params": {
                "query_string": query,
                "from": (page-1)*(constants.k),
                "size": constants.k
                }
            }

    # Sucht im gegebenen Index nach einem stringwise match im Feld text
    # Liefert ein JSON.
    response = app.elasticsearch.search_template(index=index, body=body)
    return response
