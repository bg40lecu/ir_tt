from app import app, data, constants
from app.es_interface import bulk_index, create_index, delete_index, update_mapping, update_settings, index_exists
from elasticsearch.exceptions import NotFoundError
import click
import time


def make_index(index):
    print("Creating new index: {}".format(index))
    create_index(index)
    update_settings(index)
    update_mapping(index)
    print("Indexing...")
    start = time.time()
    add_all_to_index(index)
    print('Indexing into {} done! Needed {}s'.format(index, round(time.time() - start, 2)))


@app.cli.command(help="(re)builds the index completely")
def init_index(index=constants.indexname):
    try:
        print("Deleting old index: {}".format(index))
        delete_index(index)
    except NotFoundError:
        pass

    make_index(index)


@app.cli.command(help="builds the index if it not exitst")
def init_index_if_not_exists(index=constants.indexname):
    if not index_exists(index):
        make_index(index)


@app.cli.command(help="deletes the index")
def del_index(index=constants.indexname):
    delete_index(index)


@app.cli.command(help="reloads the index settings (for testings)")
def reload_settings(index=constants.indexname):
    update_settings(index)


def add_all_to_index(index):
    instances = data.load_all_instances()
    bulk_index(index, instances)
