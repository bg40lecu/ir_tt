from flask import render_template
from flask import jsonify
from flask import request
from app import app
from app import constants
from app.forms import QueryForm, TopicQueryForm
from app import frontend, es_interface, evaluation
from app.evaluation import Topic, load_all_topics



@app.route('/', methods=['GET'])
def index():
    form = QueryForm()
    if form.validate():
        query = form.query.data
        page = form.page.data
        if page < 1:
            page = 1
        response = es_interface.query_index(index=constants.indexname, query=query, page=page)
        hit_list = frontend.response_list_constructor(response)
        app.logger.info(response)
        took = response["took"]/1000
        num_results = response["hits"]["total"]["value"]
        last_page = int(num_results/constants.k)+1
        if page > last_page:
            page = last_page
        pagination = list(range(max(1, page-2), min(last_page, page+2)+1))
        return render_template('searchresults.html', form=form, hits=hit_list, query=query, took=took, num_results=num_results, page=page, pagination=pagination)
    else:
        return render_template('searchindex.html', form=form)


@app.route('/eval', methods=['GET', 'POST'])
def evaluate():
    form = TopicQueryForm()

    topic_dict = load_all_topics()
    topics = [(topic.id, topic.query) for _, topic in topic_dict.items()]
    topics.sort()

    if form.validate_on_submit():
        id = form.topic_id.data
        topic = Topic.load_by_id(id)
        relevances = topic.relevances
        hits = es_interface.query_index(constants.indexname, topic.query)
        hit_list = frontend.response_list_constructor(hits)
        for hit in hit_list:
            hit.tid = str(hit.tid)
        return render_template('eval.html', form=form, topics=topics, topic=topic, hits=hit_list, relevances=relevances)

    else:
        return render_template('evalindex.html', form=form, topics=topics)


@app.route('/topiceval', methods=['POST'])
def topiceval():
    try:
        topic_id = request.form['topic_id']
        document_id = request.form['document_id']
        relevance = request.form['relevance']

        evaluated_topic = Topic.load_by_id(topic_id)
        evaluated_topic.add_relevances({document_id: relevance})
        evaluated_topic.save()
    except Exception as e:
        return jsonify({"success": False})

    return jsonify({"success": True})

