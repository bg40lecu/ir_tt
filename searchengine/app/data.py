from ast import literal_eval
from os import path
import csv


def load_all_instances():
    file_path = path.dirname(path.abspath(__file__))

    # read data from transcripts.csv
    with open(file_path + "/../csv/tedtalks.csv", encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile)
        talks = list(reader)
        for talk in talks:
            talk["ratings"] = literal_eval(talk["ratings"])
            talk["related_talks"] = literal_eval(talk["related_talks"])

        return talks
