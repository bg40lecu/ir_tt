#!/bin/python

from csv import DictReader, DictWriter
from collections import OrderedDict
from threading import Thread
from collections import defaultdict
import requests
import time
import bs4


class UrlDownloader():

    # parallel fetching of urls
    class Fetcher(Thread):
        def __init__(self, tedtalks):
            super().__init__()
            self.tedtalks = tedtalks

        def run(self):
            self.urls = []
            session = requests.session()
            for key, tedtalk in self.tedtalks:
                url = UrlDownloader._embedding_generator(tedtalk["url"])
                response = session.get(url)
                # if too many requests
                while response.status_code == 429:
                    time.sleep(1.0)
                    response = session.get(url)

                self.urls.append((key, UrlDownloader._scrape_thumbnail_url(response.text)))

    def __init__(self, tedtalks, num_threads=2):
        parts = defaultdict(lambda: [])
        for i, (key, tedtalk) in enumerate(tedtalks.items()):
            parts[i % num_threads].append((key, tedtalk))
        self.parts = parts

    def download(self):
        threads = [UrlDownloader.Fetcher(part) for part in self.parts.values()]
        results = []
        for thread in threads:
            thread.start()
        for thread in threads:
            thread.join()
            results.extend(thread.urls)

        return results

    @staticmethod
    def _scrape_thumbnail_url(page):
        soup = bs4.BeautifulSoup(page, 'html.parser')
        try:
            embeddable_poster = soup.find_all('div', {'class': 'Embeddable__landing__poster'})[0]
            style = embeddable_poster['style']
            return style[21:len(style) - 1]
        # occurs when website with talk isnt on ted site anymore
        except IndexError:
            return 'https://developers.google.com/maps/documentation/maps-static/images/error-image-generic.png'


    @staticmethod
    def _embedding_generator(url):
        parts = url.split(".")
        parts[0] = 'https://embed'
        return ".".join(parts)


if __name__ == "__main__":
    outfile = "tedtalks.csv"
    merged_csv = {}

    with open("transcripts.csv", "r") as csvfile:
        reader = DictReader(csvfile)
        # pos preserve the order
        for pos, row in enumerate(reader):
            row["pos"] = pos
            merged_csv[row["url"]] = row

    with open("ted_main.csv", "r") as csvfile:
        reader = DictReader(csvfile)
        for row in reader:
            try:
                merged_csv[row["url"]].update(row)
            except Exception as e:
                pass

    print("Downloading of thumbnail urls (takes about 5 mins)...")
    start = time.time()
    downloader = UrlDownloader(merged_csv)
    results = downloader.download()
    end = time.time()
    print("Downloading done, took {:.2f}s".format(end - start))

    for key, url in results:
        merged_csv[key]["thumbnail_url"] = url

    tmp_talks = [talk for talk in merged_csv.values()]
    tmp_talks.sort(key=lambda x: x["pos"])
    talks = []
    for id, talk in enumerate(tmp_talks, start=1):
        talk_with_id = OrderedDict({"id": id})
        talk_with_id.update(talk)
        del talk_with_id["pos"]
        url = talk_with_id["url"]
        url = url.replace("\n", "")
        talk_with_id["url"] = url
        talks.append(talk_with_id)

    with open(outfile, "w") as csvfile:
        writer = DictWriter(csvfile, fieldnames=talks[0].keys())
        writer.writeheader()
        writer.writerows(talks)
    print("Wrote results to outputfile: {}".format(outfile))
